
Deprecated: array_key_exists(): Using array_key_exists() on objects is deprecated. Use isset() or property_exists() instead in phar:///usr/local/bin/phpdoc/vendor/twig/twig/lib/Twig/Template.php on line 527

Call Stack:
    0.0266     392264   1. {main}() /usr/local/bin/phpdoc:0
    0.0807    2268736   2. require('phar:///usr/local/bin/phpdoc/bin/phpdoc') /usr/local/bin/phpdoc:10
    0.3473   11163000   3. phpDocumentor\Application->run() phar:///usr/local/bin/phpdoc/bin/phpdoc:23
    0.3499   11291192   4. Cilex\Provider\Console\ContainerAwareApplication->run() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Application.php:183
    0.3501   11291192   5. Cilex\Provider\Console\ContainerAwareApplication->doRun() phar:///usr/local/bin/phpdoc/vendor/symfony/console/Symfony/Component/Console/Application.php:126
    0.3503   11292056   6. Cilex\Provider\Console\ContainerAwareApplication->doRunCommand() phar:///usr/local/bin/phpdoc/vendor/symfony/console/Symfony/Component/Console/Application.php:195
    0.3504   11292056   7. phpDocumentor\Command\Project\RunCommand->run() phar:///usr/local/bin/phpdoc/vendor/symfony/console/Symfony/Component/Console/Application.php:874
    0.3511   11294704   8. phpDocumentor\Command\Project\RunCommand->execute() phar:///usr/local/bin/phpdoc/vendor/symfony/console/Symfony/Component/Console/Command/Command.php:257
    0.7303   15569248   9. phpDocumentor\Transformer\Command\Project\TransformCommand->run() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Command/Project/RunCommand.php:275
    0.7311   15571416  10. phpDocumentor\Transformer\Command\Project\TransformCommand->execute() phar:///usr/local/bin/phpdoc/vendor/symfony/console/Symfony/Component/Console/Command/Command.php:257
    0.8624   15878272  11. phpDocumentor\Console\Output\Output->writeTimedLog() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Transformer/Command/Project/TransformCommand.php:206
    0.8629   15878272  12. call_user_func_array:{phar:///usr/local/bin/phpdoc/src/phpDocumentor/Console/Output/Output.php:59}() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Console/Output/Output.php:59
    0.8629   15878288  13. phpDocumentor\Transformer\Transformer->execute() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Console/Output/Output.php:59
    0.8673   15885768  14. phpDocumentor\Transformer\Transformer->transformProject() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Transformer/Transformer.php:141
    8.7749   19146760  15. phpDocumentor\Transformer\Transformer->applyTransformationToProject() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Transformer/Transformer.php:275
    8.7753   19146920  16. phpDocumentor\Plugin\Twig\Writer\Twig->transform() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Transformer/Transformer.php:312
    8.8106   19202600  17. Twig_Environment->render() phar:///usr/local/bin/phpdoc/src/phpDocumentor/Plugin/Twig/Writer/Twig.php:116
    8.8110   19203784  18. __TwigTemplate_cc4f4aa84f8eb3e764c78e0dc52cba28c43b1ad19652aba32158f08179f80dc1->render() phar:///usr/local/bin/phpdoc/vendor/twig/twig/lib/Twig/Environment.php:334
    8.8111   19220296  19. __TwigTemplate_cc4f4aa84f8eb3e764c78e0dc52cba28c43b1ad19652aba32158f08179f80dc1->display() phar:///usr/local/bin/phpdoc/vendor/twig/twig/lib/Twig/Template.php:366
    8.8111   19221048  20. __TwigTemplate_cc4f4aa84f8eb3e764c78e0dc52cba28c43b1ad19652aba32158f08179f80dc1->displayWithErrorHandling() phar:///usr/local/bin/phpdoc/vendor/twig/twig/lib/Twig/Template.php:355
    8.8111   19221048  21. __TwigTemplate_cc4f4aa84f8eb3e764c78e0dc52cba28c43b1ad19652aba32158f08179f80dc1->doDisplay() phar:///usr/local/bin/phpdoc/vendor/twig/twig/lib/Twig/Template.php:381
    8.8111   19221048  22. __TwigTemplate_cc4f4aa84f8eb3e764c78e0dc52cba28c43b1ad19652aba32158f08179f80dc1->getAttribute() /private/var/folders/ns/_r4n2dh52hx1zg4911x1qy_w0000gn/T/phpdoc-twig-cache/a/0/a0638909f841cbdb199d2f2085ce308211d4f28dde12a665bb3f41c4d14a146c.php:19


Variables in local scope (#22):
  $arguments = array ()
  $arrayItem = 'source'
  $call = *uninitialized*
  $class = *uninitialized*
  $e = *uninitialized*
  $ignoreStrictCheck = FALSE
  $isDefinedTest = FALSE
  $item = 'source'
  $lcItem = *uninitialized*
  $message = *uninitialized*
  $method = *uninitialized*
  $methodName = *uninitialized*
  $methods = *uninitialized*
  $object = class phpDocumentor\Descriptor\FileDescriptor { protected $hash = '097068902260e54d5a8914f21dfcd191'; protected $path = 'Door.php'; protected $source = '<?php\n\ndeclare(strict_types=1);\n\nnamespace tasks\\task40;\n\n/**\n * Class Door\n *\n * @package tasks\\task40\n */\nclass Door implements Specifications\n{\n    /** @var string $color */\n    private string $color;\n\n    /** @var string $type */\n    private string $type;\n\n    /**\n     * Door constructor.\n     *\n     * @param $color\n     * @param $type\n     */\n    public function __construct($color, $type)\n    {\n        $this->color = $color;\n        $this->type = $type;\n    }\n\n    /**\n...'; protected $namespaceAliases = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $includes = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $constants = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $functions = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $classes = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $interfaces = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $traits = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $markers = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $fqsen = ''; protected $name = 'Door.php'; protected $namespace = NULL; protected $package = class phpDocumentor\Descriptor\PackageDescriptor { protected $parent = class phpDocumentor\Descriptor\PackageDescriptor { ... }; protected $children = class phpDocumentor\Descriptor\Collection { ... }; protected $functions = class phpDocumentor\Descriptor\Collection { ... }; protected $constants = class phpDocumentor\Descriptor\Collection { ... }; protected $classes = class phpDocumentor\Descriptor\Collection { ... }; protected $interfaces = class phpDocumentor\Descriptor\Collection { ... }; protected $traits = class phpDocumentor\Descriptor\Collection { ... }; protected $fqsen = '\\Default'; protected $name = 'Default'; protected $namespace = NULL; protected $package = ''; protected $summary = ''; protected $description = ''; protected $fileDescriptor = NULL; protected $line = 0; protected $tags = class phpDocumentor\Descriptor\Collection { ... }; protected $errors = NULL; protected $inheritedElement = NULL }; protected $summary = ''; protected $description = ''; protected $fileDescriptor = NULL; protected $line = 0; protected $tags = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $errors = class phpDocumentor\Descriptor\Collection { protected $items = array (...) }; protected $inheritedElement = NULL }
  $ref = *uninitialized*
  $refMethod = *uninitialized*
  $ret = *uninitialized*
  $type = 'any'

<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Class Door
 *
 * @package tasks\task40
 */
class Door implements Specifications
{
    /** @var string $color */
    private string $color;

    /** @var string $type */
    private string $type;

    /**
     * Door constructor.
     *
     * @param $color
     * @param $type
     */
    public function __construct($color, $type)
    {
        $this->color = $color;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function open()
    {
        return 'door opened';
    }

    /**
     * @return string
     */
    public function close()
    {
        return 'door closed';
    }
}

