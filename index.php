<?php

declare(strict_types=1);

define("DELIMITER", "<br>--------------<br>");

require_once __DIR__ . '/vendor/autoload.php';

use tasks\task3hw6\ReflectionGetter;

/*
 * Task 3
 *
 * Задача 3.01: Возьмите 3-5 задач из ДЗ по ООП (в которых обязательно должны быть приватные свойства и методы) и
 * с помощью рефлексии получите доступ к ним, к результатам, что они возвращают, к их тегам/аннотациям,
 * к списку родителей и тд. Помимо этих обязательных условий, вы можете попробовать и другие,
 * описанные в документации (попробуйте хотя бы 5 разных методов).
 *
 * Задача 3.2: Используя ReflectionFunction и ReflectionParameter, извлеките информацию о методах и
 * параметрах методов в выбранных вами классах. Можете изучить не только методы классов,
 * но и обычные функции (кастомные и/или из ядра ПХП).
 *
 * Задача 3.3: Напишите логику, которая с помощью рефлексии, будет доставать пхп-анотацию у метода и
 * выводить отдельно из нее описание параметров/возвращаемый тип/исключения.
 */

$reflectionClassStudent8 = new ReflectionClass('tasks\task8\Student');

print_r($reflectionClassStudent8->getDocComment());

echo DELIMITER;

print_r($reflectionClassStudent8->getConstants());

echo DELIMITER;

print_r($reflectionClassStudent8->getProperties());

echo DELIMITER;

print_r($reflectionClassStudent8->getMethods());

echo DELIMITER;

$reflectionClassEmployee19 = new ReflectionClass('tasks\task19\Employee');

print_r($reflectionClassEmployee19->getParentClass());

echo DELIMITER;

$validateLevel = new ReflectionMethod('tasks\task19\Employee', 'validateLevel');

print_r($validateLevel->getDocComment());

echo DELIMITER;

print_r($validateLevel->isProtected());

echo DELIMITER;

print_r($validateLevel->getParameters());

echo DELIMITER;

$reflectionClassUser21 = new ReflectionClass('tasks\task21\User');

print_r($reflectionClassUser21->getConstructor());

echo DELIMITER;

print_r($reflectionClassUser21->getDocComment());

echo DELIMITER;

$constructUser21 = new ReflectionMethod('tasks\task21\User', '__construct');

print_r($constructUser21->getDocComment());

echo DELIMITER;

print_r($constructUser21->isConstructor());

echo DELIMITER;

print_r($constructUser21->isConstructor());

echo DELIMITER;

$refArrayMap = new ReflectionFunction('array_map');

print_r($refArrayMap->getParameters());

echo DELIMITER;

$constructUser21Getter = new ReflectionGetter('tasks\task21\User', '__construct');

print_r($constructUser21Getter->getAnnotation());

echo DELIMITER;

print_r($constructUser21Getter->getParam());

echo DELIMITER;

print_r($constructUser21Getter->getType());

echo DELIMITER;

print_r($constructUser21Getter->getException());

echo DELIMITER;

/*
 * Task 4
 *
 * Задача 4.01: Возьмите любую структуру данных из презентации и
 * используя ее (ваша кастомная реализация или реализация из SPL):
 * создайте эту структуру;
 * заполните ее данными (на ваше усмотрение), минимум 25 элементов;
 * достаньте несколько элементов;
 * добавьте еще несколько элементов;
 * определите (если есть такая возможность), где теперь находится
 * указатель (то есть текущее положение последнего элемента).
 */
$stack = new SplStack();

$numbers = range(1, 26);
shuffle($numbers);

foreach ($numbers as $number) {
    $stack->push($number);
}

echo $stack->pop();

echo DELIMITER;

echo $stack->pop();

echo DELIMITER;

echo $stack->pop();

echo DELIMITER;

$stack->push(33);
$stack->push(51);
$stack->push(42);

echo $stack->top();

echo DELIMITER;

/*
 * task 5
 *
 * Задача 5.01: Возьмите любые 2 из SPL
 * функций (именно функций, отдельных SPL функций, НЕ SPL классов/интерфейсов/ваших кастомных) и
 * напишите по 2 примера на каждую, как ее можно использовать (копировать примеры из оф. доки запрещается).
 */
$class = 'tasks\task19\Employee';
$parentClasses = class_parents($class);

if (isset($parentClasses)) {
    foreach ($parentClasses as $parentClass) {
        echo 'Parent class: ' . $parentClass . '<br>';
    }
}

echo DELIMITER;

$useTraits = class_uses('tasks\task33\Test');

if (isset($useTraits)) {
    foreach ($useTraits as $useTrait) {
        echo 'Class use trait: ' . $useTrait . '<br>';
    }
}

echo DELIMITER;

/*
 * Task 6
 *
 * Задача 6.01: Возьмите любой массив и с помощью ArrayIterator проитерируйте его.
 *
 * Задача 6.2: Используйте 2 любых метода, доступных в классе ArrayIterator.
 */
$arr = [3, 5, 7, 8, 9, 2, 4];
$newIterator = new ArrayIterator($arr);
$newIterator->append(20);
$newIterator->append(42);
$newIterator->asort();

echo $newIterator->count();

echo DELIMITER;

print_r($newIterator);

echo DELIMITER;

/*
 * Task 7
 *
 * Задача 7.01: Возьмите любой многомерный массив и с помощью RecursiveArrayIterator проитерируйте его.
 *
 * Задача 7.2: Используйте 2 любых метода, доступных в классе RecursiveArrayIterator.
 */
$multiArray = [
    'servers' => [
        'server1' => '127.0.0.1',
        'server2' => '123.4.5.2',
        'server6' => '134.23.45.6',
        'server7' => '145.54.65.3',
        'server3' => '201.22.43.32',
        'server5' => '178.43.22.56'
    ],
    'category' => [
        23 => 'catName1',
        34 => 'catName3',
        22 => 'catName5',
        54 => 'catName7',
        15 => 'catName2',
        28 => 'catName6'
    ],
    'fruits' => [
        'banana' => 30,
        'lemon' => 60,
        'blueberry' => 43,
        'chery' => 57,
        'apple' => 22
    ]
];

$iterator = new RecursiveArrayIterator($multiArray);

print_r($iterator);

echo DELIMITER;

$iterator->ksort();

print_r($iterator);

echo DELIMITER;

print_r($iterator->serialize());

echo DELIMITER;

/*
 * Task 8
 *
 * Задача 8.01: Создайте папку с подпапками и файлами и с помощью
 * DirectoryIterator и/или RecursiveDirectoryIterator проитерируйте его.
 *
 * Задача 8.2: Используйте 2 любых метода, доступных в классе DirectoryIterator и/или RecursiveDirectoryIterator.
 */
$path = 'img';
$directory = new RecursiveDirectoryIterator($path);
$iterator = new RecursiveIteratorIterator($directory);
$png = [];
$jpg = [];
foreach ($iterator as $info) {
    if ($info->isDir()) {
        continue;
    }

    $file = new SplFileInfo($info->getPathname());

    if ('png' === $file->getExtension()) {
        $png[] = $info->getPathname();
    }

    if ('jpg' === $file->getExtension()) {
        $jpg[] = $info->getPathname();
    }

    unset($info);
}

print_r($png);

echo DELIMITER;

print_r($jpg);

echo DELIMITER;

foreach ($directory as $it) {
    echo $it->getRealPath() . ' -- ' . $it->getPathname() . ' -- ' . $it->getType() . '<br>';
}
