<?php

declare(strict_types=1);

namespace tasks\task30;

/**
 * Class Figure
 *
 * @package tasks\task30
 */
abstract class Figure
{
    /**
     * @return int
     */
    abstract public function getSquare();

    /**
     * @return int
     */
    abstract public function getPerimeter();

    /**
     * @return int
     */
    abstract public function getSumSP();
}
