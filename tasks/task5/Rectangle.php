<?php

declare(strict_types=1);

namespace tasks\task5;

/**
 * Class Rectangle
 * @package tasks\task5
 */
class Rectangle
{
    /** @var float $width */
    public float $width;

    /** @var float $height */
    public float $height;

    /** @return float */
    public function getSquare(): float
    {
        return $this->width * $this->height;
    }

    /** @return float */
    public function getPerimeter(): float
    {
        return 2 * ($this->width + $this->height);
    }
}
