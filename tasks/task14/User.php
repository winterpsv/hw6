<?php

declare(strict_types=1);

namespace tasks\task14;

/**
 * Class User
 * @package tasks\task14
 */
class User
{
    /** @var string $surname */
    private string $surname;

    /** @var string $name */
    private string $name;

    /** @var string $patronymic */
    private string $patronymic;

    /**
     * @param string $surname
     * @return object
     */
    public function setSurname(string $surname): object
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @param string $name
     * @return object
     */
    public function setName(string $name): object
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $patronymic
     * @return object
     */
    public function setPatronymic(string $patronymic): object
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->surname[0] . $this->name[0] . $this->patronymic[0];
    }
}
