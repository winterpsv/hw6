<?php

declare(strict_types=1);

namespace tasks\task31;

/**
 * Interface Figure
 *
 * @author Sergey Pedchenko <winterpsv@gmail.com>
 *
 * @license https://opensource.org/licenses/MIT The MIT License
 *
 * @package tasks\task31
 */
interface Figure
{
    /**
     * @return mixed
     */
    public function getSquare();

    /**
     * @return mixed
     */
    public function getPerimeter();

    /**
     * @return mixed
     */
    public function getSumSP();
}
