<?php

declare(strict_types=1);

namespace tasks\task31;

/**
 * Class Triangle
 * @package tasks\task31
 */
final class Triangle implements Figure
{
    /** @var int $sideA */
    private int $sideA;

    /** @var int $sideB */
    private int $sideB;

    /** @var int $sideC */
    private int $sideC;

    /**
     * Triangle constructor.
     * @param $sideA
     * @param $sideB
     * @param $sideC
     */
    public function __construct($sideA, $sideB, $sideC)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
        $this->sideC = $sideC;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        $p = 0.5 * ($this->sideA + $this->sideB + $this->sideC);
        return sqrt($p * ($p - $this->sideA) * ($p - $this->sideB) * ($p - $this->sideC));
    }

    /**
     * @return int
     */
    public function getPerimeter(): int
    {
        return $this->sideA + $this->sideB + $this->sideC;
    }

    /**
     * @return int
     */
    public function getSumSP(): int
    {
        return $this->getSquare() + $this->getPerimeter();
    }
}
