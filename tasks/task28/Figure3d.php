<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Interface Figure3d
 *
 * @package tasks\task28
 */
interface Figure3d
{
    /**
     * @return int
     */
    public function getVolume();

    /**
     * @return int
     */
    public function getSurfaceSquare();
}
