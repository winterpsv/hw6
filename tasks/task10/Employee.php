<?php

declare(strict_types=1);

namespace tasks\task10;

/**
 * Class Employee
 * @package tasks\task10
 */
class Employee
{
    /** @var string $name */
    private string $name;

    /** @var string $surname */
    private string $surname;

    /** @var int $salary */
    private int $salary;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

    /**
     * @param int $salary
     */
    public function setSalary(int $salary): void
    {
        $this->salary = $salary;
    }
}
