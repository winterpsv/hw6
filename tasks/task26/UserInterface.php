<?php

declare(strict_types=1);

namespace tasks\task26;

/**
 * Interface UserInterface
 * 
 * @package tasks\task26
 */
interface UserInterface
{
    /**
     * UserInterface constructor.
     * 
     * @param $name
     * @param $age
     */
    public function __construct($name, $age);

    /**
     * @return string
     */
    public function getName();

    /**
     * @return int
     */
    public function getAge();
}
