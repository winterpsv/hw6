<?php

declare(strict_types=1);

namespace tasks\task4;

/**
 * Class Employee
 * @package tasks\task4
 */
class Employee
{
    /** @var string $name */
    public string $name;

    /** @var int $salary */
    public int $salary;

    /** Set $salary*2 */
    public function doubleSalary(): void
    {
        $this->salary = $this->salary * 2;
    }
}
