<?php

declare(strict_types=1);

namespace tasks\task24;

/**
 * Interface UserInterface
 *
 * @package tasks\task24
 */
interface UserInterface
{
    /**
     * @param $name
     */
    public function setName($name); // установить имя

    /**
     * @return mixed
     */
    public function getName(); // получить имя

    /**
     * @param $age
     */
    public function setAge($age); // установить возраст

    /**
     * @return mixed
     */
    public function getAge(); // получить возраст
}
