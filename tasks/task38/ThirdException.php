<?php

declare(strict_types=1);

namespace tasks\task38;

/**
 * Class ThirdException
 *
 * @package tasks\task38
 */
class ThirdException extends \Exception
{
}
