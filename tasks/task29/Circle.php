<?php

declare(strict_types=1);

namespace tasks\task29;

use tasks\task28\Figure as Figure28;

/**
 * Interface Circle
 *
 * @package tasks\task29
 */
interface Circle extends Figure28
{
    /** @var float Pi */
    public const P = 3.1415;

    /**
     * @return int
     */
    public function getRadius();

    /**
     * @return int
     */
    public function getDiameter();
}
