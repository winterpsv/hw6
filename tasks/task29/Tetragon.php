<?php

declare(strict_types=1);

namespace tasks\task29;

/**
 * Interface Tetragon
 *
 * @package tasks\task29
 */
interface Tetragon
{
    /**
     * @return int
     */
    public function getSideA();

    /**
     * @return int
     */
    public function getSideB();

    /**
     * @return int
     */
    public function getSideC();

    /**
     * @return int
     */
    public function getSideD();
}
