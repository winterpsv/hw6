<?php

declare(strict_types=1);

namespace tasks\task41;

/**
 * Class ErrHandler
 *
 * @package tasks\task41
 */
class ErrHandler
{
    /**
     * ErrHandler constructor.
     */
    public function __construct()
    {
        ob_start();
        set_error_handler(array($this, 'OtherError'));
        register_shutdown_function(array($this, 'FatalError'));
    }

    /**
     * @param $errno
     * @param $errstr
     * @param $file
     * @param $line
     *
     * @return bool
     */
    public function OtherError($errno, $errstr, $file, $line)
    {
        error_log(
            $errno . ' - ' . $errstr . ' - ' . $file . ' - ' . $line . PHP_EOL,
            3,
            $_SERVER['DOCUMENT_ROOT'] . '/logs.log'
        );
        return true;
    }

    /**
     * Fatal error handler
     */
    public function FatalError()
    {
        $error = error_get_last();
        if (isset($error)) {
            if ($error['type'] == E_ERROR
                || $error['type'] == E_PARSE
                || $error['type'] == E_COMPILE_ERROR
                || $error['type'] == E_CORE_ERROR) {
                ob_end_clean();
                error_log(
                    $error['type'] . ' - ' . $error['message'] . PHP_EOL,
                    3,
                    $_SERVER['DOCUMENT_ROOT'] . '/logs.log'
                );
            } else {
                ob_end_flush();
            }
        } else {
            ob_end_flush();
        }
    }
}
