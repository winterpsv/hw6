<?php

declare(strict_types=1);

namespace tasks\task9;

/**
 * Class Employee
 *
 * @package tasks\task9
 */
class Employee
{
    /** @var string $name */
    private string $name;

    /** @var int $age */
    private int $age;

    /** @var int $salary */
    private int $salary;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary . '$';
    }

    /**
     * @param string $newName
     */
    public function setName(string $newName): void
    {
        $this->name = $newName;
    }

    /**
     * @param int $newAge
     */
    public function setAge(int $newAge): void
    {
        (!$this->isAgeCorrect($newAge)) ?: $this->age = $newAge;
    }

    /**
     * @param int $newSalary
     */
    public function setSalary(int $newSalary): void
    {
        $this->salary = $newSalary;
    }

    /**
     * @param int $age
     *
     * @return bool
     */
    private function isAgeCorrect(int $age): bool
    {
        return ($age >= 1 && $age <= 100) ? true : false;
    }
}
