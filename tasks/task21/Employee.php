<?php

declare(strict_types=1);

namespace tasks\task21;

use Exception;

/**
 * Class Employee
 *
 * @package tasks\task21
 */
class Employee extends User
{
    /** @var int $salary */
    private int $salary;

    /**
     * Employee constructor.
     *
     * @param string $name
     * @param string $surname
     * @param string $date
     * @param int $salary
     *
     * @throws Exception
     */
    public function __construct(string $name, string $surname, string $date, int $salary)
    {
        parent::__construct($name, $surname, $date);
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}
