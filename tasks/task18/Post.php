<?php

declare(strict_types=1);

namespace tasks\task18;

/**
 * Class Post
 * 
 * @package tasks\task18
 */
class Post
{
    /** @var string $name */
    private string $name;

    /** @var int $salary */
    private int $salary;

    /**
     * Post constructor.
     * @param string $name
     * @param int $salary
     */
    public function __construct(string $name, int $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}
