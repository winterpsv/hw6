<?php

declare(strict_types=1);

namespace tasks\task15;

use tasks\task15\Product as Task15;

/**
 * Class Cart
 *
 * @package tasks\task15
 */
class Cart
{
    /** @var array $products */
    private array $products = [];

    /**
     * @param Product $product
     */
    public function add(Task15 $product): void
    {
        $this->products[] = $product;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function remove(string $name): bool
    {
        foreach ($this->products as $key => $product) {
            if ($product->getName() === $name) {
                unset($this->products[$key]);
                return true;
            }
        }
        return false;
    }

    /**
     * @return int
     */
    public function getTotalCost(): int
    {
        $totalCost = 0;
        foreach ($this->products as $product) {
            $totalCost = $totalCost + $product->getCost();
        }
        return $totalCost;
    }

    /**
     * @return int
     */
    public function getTotalQuantity(): int
    {
        $totalQuantity = 0;
        foreach ($this->products as $product) {
            $totalQuantity = $totalQuantity + $product->getQuantity();
        }
        return $totalQuantity;
    }

    /**
     * @return float
     */
    public function getAvgPrice(): float
    {
        $avgPrice = 0;
        if (0 !== $this->getTotalQuantity()) {
            $avgPrice = $this->getTotalCost() / $this->getTotalQuantity();
        }
        return $avgPrice;
    }
}
