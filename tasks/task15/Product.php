<?php

declare(strict_types=1);

namespace tasks\task15;

/**
 * Class Product
 *
 * @package tasks\task15
 */
class Product
{
    /** @var string $name */
    private string $name;

    /** @var int $price */
    private int $price;

    /** @var int $quantity */
    private int $quantity;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->price * $this->quantity;
    }
}
