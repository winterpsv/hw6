<?php

declare(strict_types=1);

namespace tasks\task6;

/**
 * Class User
 *
 * @package tasks\task6
 */
class User
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @param int $newAge */
    public function setAge(int $newAge): void
    {
        (!$this->validateAge($newAge)) ?: $this->age = $newAge;
    }

    /** @param int $age */
    public function addAge(int $age): void
    {
        $newAge = $this->age + $age;
        (!$this->validateAge($newAge)) ?: $this->age = $newAge;
    }

    /** @param int $age */
    public function subAge(int $age): void
    {
        $newAge = $this->age - $age;
        (!$this->validateAge($newAge)) ?: $this->age = $newAge;
    }

    /**
     * @param int $age
     *
     * @return bool
     */
    private function validateAge(int $age): bool
    {
        return ($age >= 18) ? true : false;
    }
}
