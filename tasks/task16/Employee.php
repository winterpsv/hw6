<?php

declare(strict_types=1);

namespace tasks\task16;

/**
 * Class Employee
 *
 * @package tasks\task16
 */
class Employee
{
    /** @var string $name */
    public string $name;

    /** @var int $salary */
    public int $salary;
}
