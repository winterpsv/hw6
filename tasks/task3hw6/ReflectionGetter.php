<?php

declare(strict_types=1);

namespace tasks\task3hw6;

/**
 * Class ReflectionGetter
 *
 * @package tasks\task3hw6
 */
class ReflectionGetter
{
    /** @var string */
    private string $annotation;

    /**
     * ReflectionGetter constructor.
     *
     * @param string $class
     * @param string $method
     *
     * @throws \ReflectionException
     */
    public function __construct(string $class, string $method)
    {
        $constructUser21 = new \ReflectionMethod($class, $method);

        $this->annotation = $constructUser21->getDocComment();
    }

    /**
     * @return string
     */
    public function getAnnotation(): string
    {
        return $this->annotation;
    }

    /**
     * @return array
     */
    public function getParam(): array
    {
        return $this->getElements($this->getAnnotation(), '@param');
    }

    /**
     * @return array
     */
    public function getType(): array
    {
        return $this->getElements($this->getAnnotation(), '@return');
    }

    /**
     * @return array
     */
    public function getException(): array
    {
        return $this->getElements($this->getAnnotation(), '@throws');
    }

    /**
     * Pars annotation and return cur element
     *
     * @param $str
     * @param string $tag
     *
     * @return array
     */
    private function getElements($str, $tag = '')
    {
        if (empty($tag)) {
            return $str;
        }

        $matches = [];
        preg_match_all('/' . $tag . '(.*)(\\r\\n|\\r|\\n)/U', $str, $matches);

        if (isset($matches[1])) {
            return $matches[1];
        }

        return [];
    }
}
