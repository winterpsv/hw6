<?php

declare(strict_types=1);

namespace tasks\task1;

/**
 * Class Employee
 * @package tasks\task1
 */
class Employee
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @var int $salary */
    public int $salary;
}
