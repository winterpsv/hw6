<?php

declare(strict_types=1);

namespace tasks\task25;

/**
 * Interface CubeInterface
 *
 * @package tasks\task25
 */
interface CubeInterface
{
    /**
     * CubeInterface constructor.
     * 
     * @param $side
     */
    public function __construct($side);

    /**
     * @return int
     */
    public function getVolumeCube();

    /**
     * @return int
     */
    public function getSurfaceArea();
}
