<?php

declare(strict_types=1);

namespace tasks\task27;

/**
 * Interface EmployeeInterface
 *
 * @package tasks\task27
 */
interface EmployeeInterface extends UserInterface
{
    /**
     * @param $salary
     */
    public function setSalary($salary);

    /**
     * @return int
     */
    public function getSalary();
}
