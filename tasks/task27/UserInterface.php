<?php

declare(strict_types=1);

namespace tasks\task27;

/**
 * Interface UserInterface
 * 
 * @package tasks\task27
 */
interface UserInterface
{
    /**
     * @param $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $age
     */
    public function setAge($age);

    /**
     * @return int
     */
    public function getAge();
}
