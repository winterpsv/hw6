<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Interface Specifications
 *
 * @copyright Copyright (c) 2020, Sergey Pedchenko
 *
 * @package tasks\task40
 */
interface Specifications
{
    /**
     * @return mixed
     */
    public function open();

    /**
     * @return mixed
     */
    public function close();
    //other functionality
}
