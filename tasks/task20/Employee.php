<?php

declare(strict_types=1);

namespace tasks\task20;

use tasks\task8\Student as Student8;

/**
 * Class Employee
 *
 * @package tasks\task20
 */
class Employee extends Student8
{
    /** @var string $courseAdministratorPatronymic */
    private string $courseAdministratorPatronymic;

    /**
     * @return string
     */
    public function getCourseAdministratorPatronymic(): string
    {
        return $this->courseAdministratorPatronymic;
    }

    /**
     * @param string $courseAdministratorPatronymic
     */
    public function setCourseAdministratorPatronymic(string $courseAdministratorPatronymic): void
    {
        $this->courseAdministratorPatronymic = $courseAdministratorPatronymic;
    }

    /**
     * @return bool
     */
    public function validateCourseAdministrator(): bool
    {
        if(!parent::validateCourseAdministrator()) {
            return false;
        }

        if (!preg_match(self::PATTERN, $this->courseAdministratorPatronymic)) {
            return false;
        }

        return true;
    }
}